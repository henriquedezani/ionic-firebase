import { CadastroPage } from './../pages/cadastro/cadastro';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListaPage } from './../pages/lista/lista';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';

const ambiente = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCXL_mu6XhI5yTRx6BGuAh8CetRGpvKmuw",
    authDomain: "tarefas-3cae0.firebaseapp.com",
    databaseURL: "https://tarefas-3cae0.firebaseio.com",
    projectId: "tarefas-3cae0",
    storageBucket: "tarefas-3cae0.appspot.com",
    messagingSenderId: "862508479900"
  }
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListaPage,
    CadastroPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(ambiente.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListaPage,
    CadastroPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
