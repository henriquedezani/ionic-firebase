import { CadastroPage } from './../cadastro/cadastro';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'page-lista',
  templateUrl: 'lista.html',
})
export class ListaPage {

  tarefas: Observable<any[]>;

  constructor(public navCtrl: NavController, public db: AngularFirestore) {
    this.tarefas = db.collection('tarefas').valueChanges();
  }

  public delete(id: string): void {
    this.db.collection('tarefas').doc(id).delete();
  }

  public adicionar(): void {
    this.navCtrl.push(CadastroPage);
  }

}
